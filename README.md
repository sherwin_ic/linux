Original LKL Readme can be found [here](Documentation/lkl.txt)

# Morello LKL

This is the Morello LKL project, which combines the [Morello Linux](https://git.morello-project.org/morello/kernel/linux) and [Linux Kernel Library (LKL)](https://github.com/lkl/linux) projects to create a userspace librayOS that supports the [PCuABI](https://git.morello-project.org/morello/kernel/linux/-/wikis/Morello-pure-capability-kernel-user-Linux-ABI-specification) and runs a purecap application in the same address space as the librayOS. This project uses a [modified libc](https://git.morello-project.org/sherwin_ic/morello-lkl-musl).

The bulk of the libraryOS is executed as a hybrid shared library, while the application and a small part of the LKL library is linked together into a purecap binary. The Morello Musl C library has been modified to support this project, and can be found in a separate repo. The purecap part of the project (and most of the modified libc) executes in the Restricted state, having access to a separate stack register and DDC (set to null), while the hybrid Morello-LKL libraryOS runs in the Executive state and can also access the regular capability stack pointer and DDC.

# Building

## Morello Toolchain

This project was built with release 1.5 of the Morello LLVM toolchain. It is eaiest to get the [prebuilt binary](https://git.morello-project.org/morello/llvm-project-releases/-/commit/a6fd8d758d2534b59a53221c0419350ddcf615f0).

The toolchain can also be built from source following [these instructions](https://git.morello-project.org/morello/musl-libc/-/blob/cea12937f4e553406860d8e5e7e9cb265d58481b/build-morello-clang.rst).

## Host Operating System - Morello Linux FVP/SoC

This project has been tested on release 1.5 of the Morello Linux Kernel. It may not work on future versions, when for example, purecap binaries are loaded with the DDC set to NULL (This would then require the dynamic loader to be changed to set its own DDC).

The host Debian purecap Morello Linux can be built for either the SoC or FVP by following the user guide [here](https://git.morello-project.org/morello/docs/-/blob/ea21d403367309e043c7136f490ceddbf4a83945/user-guide.rst). If building for the FVP it is recommended to use the VirtioP9 Device to share a folder between Morello Linux running on the FVP and the x86 host Linux platform.

## Morello Musl

This project requires the modified Musl repo provided alongside this project, which can be found [here](https://git.morello-project.org/sherwin_ic/morello-lkl-musl).

Use the [build script](https://git.morello-project.org/morello/musl-libc/-/blob/cedc8adc212251c7585b79d607501906aa683053/tools/build-morello.sh) (also included in the provided repo) to build Morello Musl, following the instructions [here](https://git.morello-project.org/morello/musl-libc/-/blob/cea12937f4e553406860d8e5e7e9cb265d58481b/build-morello-clang.rst). Only the "Build Musl" step is needed if the prebuilt Morello toolchain is being used.

Hence only this command would be needed:
```
MORELLO_NPROC=4 CC=/path/to/clang bash build-morello.sh musl /path/to/morello-lkl-musl-repo /path/to/sysroot aarch64-unknown-linux-musl_purecap
```
Note that `/path/to/sysroot` is where the built Musl lib and include files would be installed. WARNING: This whole directory provided would be deleted by the build script at the beginning! This path will be the `SYSROOT` folder mentioned later

## Linux Header Files

These files are needed to build Morello LKL. Checkout the repo from [here](https://git.morello-project.org/morello/morello-linux-headers/-/tree/morello-release-1.5.0), then enter the `usr` directory and copy the include folder to the same `SYSROOT` folder:
```
cd morello-linux-headers/usr
cp -r include/ /path/to/sysroot/
```

## Morello-LKL

With the Morello Toolchain bin folder on your path (containing clang, ld.lld, llvm-ar etc), simply type:
```
make ARCH=arm64 "-j$(nproc)" CROSS_COMPILE="aarch64-none-linux-gnu-" SYSROOT=/path/to/sysroot -C tools/lkl
```
Replace `$(nproc)` with the number of cores you would like to use for compilation.

You can also override tha compiler, linker and archiver by giving its full path:
```
make ARCH=arm64 "-j$(nproc)" CROSS_COMPILE="aarch64-none-linux-gnu-" SYSROOT=/path/to/sysroot \
	CC=/path/to/clang LD=/path/to/linker/ld.lld AR=/path/to/llvm-ar -C tools/lkl
```


# Running Tests

In this porject all 35/35 basic boot tests provided with the original LKL project passes. The remaining tests that focus on network or disk functionality have not been run. The passing boot tests perform the following operations in a small test program linked to LKL:
 - Booting kernel
 - Performing Syscalls
 - Creating, mounting filesystem
 - Read, write operations
 - Launching multiple threads

After compilation, the built tests would be in `tools/lkl/tests`. At this point, the `SYSROOT`/`lib` folder would have `libc.so` (from Musl) and `liblkl.so` (from Morello LKL).

Copy the `boot` binary in `tools/lkl/tests` to the Morello system (FVP or SoC). Also copy the `SYSROOT` folder here as well, taking care to keep `SYSROOT` file path the same. This would be made easier if this folder has been shared between the build environment and the Morello environment.

Run the boot tests:
```
./boot
```

If the interpreter path was not found or if it is not possible to keep the `SYSROOT` paths the same on both systems, try running the file directly from the interpreter:
```
/path/to/built/libc.so /path/to/test/file/boot
```
