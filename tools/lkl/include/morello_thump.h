/* This file provides a function veneers for calling hybrid functions from purecap ones */

__uintcap_t __call_lkl_syscall(long no, void *params);
int __call_lkl_start_kernel(char *bootcmdline);
long __call_lkl_sys_halt(void);
int __call_lkl_init(void *lkl_ops);
void __call_lkl_cleanup(void);
int __call_lkl_trigger_irq(int irq);
int __call_lkl_get_free_irq(void *user);
void __call_lkl_put_irq(int irq, void *name);
int __call_lkl_is_running(void);
void __call_hybrid_fn(void *);

void __ret_from_hybrid_fn(void);

struct call_hybrid_fn {
	void *arg;
	void (*fn)(void *);
};

#define NOT_C64_FN(fn) (!(__builtin_cheri_address_get(fn) & 1))
