#include <setjmp.h>
#include <lkl_host.h>

void jmp_buf_set(struct lkl_jmp_buf *jmpb, void (*f)(void))
{
	if (!setjmp(*((jmp_buf *)jmpb->buf))) {
		struct call_hybrid_fn fn_call;
		fn_call.arg = 0;
		fn_call.fn = (void (*)(void *))f;
		__call_hybrid_fn(&fn_call);
	}
}

void jmp_buf_longjmp(struct lkl_jmp_buf *jmpb, int val)
{
	longjmp(*((jmp_buf *)jmpb->buf), val);
}
