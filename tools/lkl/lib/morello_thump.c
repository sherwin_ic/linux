/* This file provides a function veneers for calling hybrid functions from purecap ones */
#include <lkl.h>

#define A64_FUNC_CALL(func_name)						\
__asm__(									\
".text \n"									\
".global __call_"#func_name "\n"						\
".type  __call_"#func_name ",%function\n"					\
".extern __ret_from_hybrid_fn\n"						\
"__call_"#func_name ":\n"							\
	/* Save Callee-saved capability registers. These may be invalidated by 	\
	 * hybrid code that would save x registers instead of c registers. 	\
	 */									\
	"stp c19, c20, [csp, #-32]!\n"						\
	"stp c21, c22, [csp, #-32]!\n"						\
	"stp c23, c24, [csp, #-32]!\n"						\
	"stp c25, c26, [csp, #-32]!\n"						\
	"stp c27, c28, [csp, #-32]!\n"						\
	"stp c29, c30, [csp, #-32]!\n"						\
	"add c29, csp, #16\n"							\
										\
	"adrp c30, :got:__ret_from_hybrid_fn\n"					\
	"ldr c30, [c30, :got_lo12:__ret_from_hybrid_fn]\n"			\
	/* Branches to the LKL kernel through a PLT,				\
	 * and switches execution to the Executive state			\
	 */									\
	"b "#func_name"\n" 							\
	".size __call_"#func_name ", .-__call_"#func_name "\n"			\
);

A64_FUNC_CALL(lkl_syscall);
A64_FUNC_CALL(lkl_start_kernel);
A64_FUNC_CALL(lkl_sys_halt);
A64_FUNC_CALL(lkl_init);
A64_FUNC_CALL(lkl_cleanup);
A64_FUNC_CALL(lkl_trigger_irq);
A64_FUNC_CALL(lkl_get_free_irq);
A64_FUNC_CALL(lkl_put_irq);
A64_FUNC_CALL(lkl_is_running);


__asm__(
".text \n"
".global __call_hybrid_fn\n"
".type  __call_hybrid_fn, %function\n"
".extern __ret_from_hybrid_fn\n"
"__call_hybrid_fn:\n"
	/* Save Callee-saved capability registers. These may be invalidated by 
	 * hybrid code that would save x registers instead of c registers.
	 * Make use of rcsp to preserve csp.
	 */
	"stp c19, c20, [csp, #-32]!\n"
	"stp c21, c22, [csp, #-32]!\n"
	"stp c23, c24, [csp, #-32]!\n"
	"stp c25, c26, [csp, #-32]!\n"
	"stp c27, c28, [csp, #-32]!\n"
	"stp c29, c30, [csp, #-32]!\n"
	"add c29, csp, #16\n"
	/* load function and args from call_hybrid_fn struct */
	"ldr c1, [c0, #16]\n"
	"ldr c0, [c0]\n"
	"adrp c30, :got:__ret_from_hybrid_fn\n"
	"ldr c30, [c30, :got_lo12:__ret_from_hybrid_fn]\n"
	"br c1\n"
	".size __call_hybrid_fn, .-__call_hybrid_fn\n"
);
