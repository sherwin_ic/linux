#include <asm/host_ops.h>

int lkl_longjmp(void *__capability buf, int val);
int lkl_setjmp(void *__capability buf);

void lkl_jmp_buf_set(struct lkl_jmp_buf *__capability jmpb, void (*f)(void))
{
	if(!lkl_setjmp(jmpb->buf)) {
		f();
	}
}

void lkl_jmp_buf_longjmp(struct lkl_jmp_buf *__capability jmpb, int val)
{
	lkl_longjmp(jmpb->buf, val);
}

/**
 * __call_lkl_ops - Call an lkl_ops function
 * 
 * This veneer allows the lkl kernel, running in A64 mode to call
 * a function in C64 space in restricted mode.
 * 
 * This works by storing many copies of the below asm function in the
 * lkl_ops struct that redirects the call to the correct host lkl
 * function that was passed in through lkl_init()
 */

struct lkl_host_operations *__capability real_lkl_ops;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wstrict-prototypes"
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wasm-operand-widths"
typedef void *__capability (*__capability FnPtr)();

#define LKL_OPS_CALL(num)							\
void *__capability __call_lkl_ops_##num(void *__capability a1,			\
	void *__capability a2,void *__capability a3,void *__capability a4,	\
	void *__capability a5,void *__capability a6)				\
{										\
	void *__capability ret;							\
	FnPtr f = ((FnPtr *__capability)real_lkl_ops)[num];			\
	__asm__ (								\
		"mov c0, %1\n"							\
		"mov c1, %2\n"							\
		"mov c2, %3\n"							\
		"mov c3, %4\n"							\
		"mov c4, %5\n"							\
		"mov c5, %6\n"							\
		"blrr %7\n" /* Switch to Restricted state */			\
		"mov %0, c0\n"							\
	: "=r"(ret)								\
	: "r"(a1), "r"(a2), "r"(a3), "r"(a4), "r"(a5), "r"(a6), "r"(f)		\
	: "c0", "c1", "c2", "c3", "c4", "c5", "c6", "c30");			\
	return ret;								\
}

LKL_OPS_CALL(0);
LKL_OPS_CALL(1);
LKL_OPS_CALL(2);
LKL_OPS_CALL(3);
LKL_OPS_CALL(4);
LKL_OPS_CALL(5);
LKL_OPS_CALL(6);
LKL_OPS_CALL(7);
LKL_OPS_CALL(8);
LKL_OPS_CALL(9);
LKL_OPS_CALL(10);
LKL_OPS_CALL(11);
LKL_OPS_CALL(12);
LKL_OPS_CALL(13);
LKL_OPS_CALL(14);
LKL_OPS_CALL(15);
LKL_OPS_CALL(16);
LKL_OPS_CALL(17);
LKL_OPS_CALL(18);
LKL_OPS_CALL(19);
LKL_OPS_CALL(20);
LKL_OPS_CALL(21);
LKL_OPS_CALL(22);
LKL_OPS_CALL(23);
LKL_OPS_CALL(24);
LKL_OPS_CALL(25);
LKL_OPS_CALL(26);
LKL_OPS_CALL(27);
LKL_OPS_CALL(28);
LKL_OPS_CALL(29);
LKL_OPS_CALL(30);
LKL_OPS_CALL(31);
LKL_OPS_CALL(32);


LKL_OPS_CALL(35);
LKL_OPS_CALL(36);
LKL_OPS_CALL(37);
LKL_OPS_CALL(38);

#define LKL_PCI_OPS_CALL(num)							\
void *__capability __call_lkl_pci_ops_##num(void *__capability a1,		\
	void *__capability a2,void *__capability a3,void *__capability a4,	\
	void *__capability a5,void *__capability a6)				\
{										\
	void *__capability ret;							\
	FnPtr f = ((FnPtr *__capability)real_lkl_ops->pci_ops)[num];		\
	__asm__ (								\
		"mov c0, %1\n"							\
		"mov c1, %2\n"							\
		"mov c2, %3\n"							\
		"mov c3, %4\n"							\
		"mov c4, %5\n"							\
		"mov c5, %6\n"							\
		"blrr %7\n" /* Switch to Restricted state */			\
		"mov %0, c0\n"							\
	: "=r"(ret)								\
	: "r"(a1), "r"(a2), "r"(a3), "r"(a4), "r"(a5), "r"(a6), "r"(f)		\
	: "c0", "c1", "c2", "c3", "c4", "c5", "c6", "c30");			\
	return ret;								\
}

LKL_PCI_OPS_CALL(0);
LKL_PCI_OPS_CALL(1);
LKL_PCI_OPS_CALL(2);
LKL_PCI_OPS_CALL(3);
LKL_PCI_OPS_CALL(4);
LKL_PCI_OPS_CALL(5);
LKL_PCI_OPS_CALL(6);
LKL_PCI_OPS_CALL(7);
#pragma clang diagnostic pop
#pragma clang diagnostic pop



struct lkl_host_operations lkl_ops_veneer;
struct lkl_dev_pci_ops lkl_pci_ops_veneer;

void init_lkl_ops(void) {
	void **pci = (void **) &lkl_pci_ops_veneer;
	void **p = (void **) &lkl_ops_veneer;

	pci[2*0] = (void *)__call_lkl_pci_ops_0;
	pci[2*1] = (void *)__call_lkl_pci_ops_1;
	pci[2*2] = (void *)__call_lkl_pci_ops_2;
	pci[2*3] = (void *)__call_lkl_pci_ops_3;
	pci[2*4] = (void *)__call_lkl_pci_ops_4;
	pci[2*5] = (void *)__call_lkl_pci_ops_5;
	pci[2*6] = (void *)__call_lkl_pci_ops_6;
	pci[2*7] = (void *)__call_lkl_pci_ops_7;

	lkl_ops_veneer.virtio_devices = real_lkl_ops->virtio_devices;
	p[2*1] = (void *)__call_lkl_ops_1;
	p[2*2] = (void *)__call_lkl_ops_2;
	p[2*3] = (void *)__call_lkl_ops_3;
	p[2*4] = (void *)__call_lkl_ops_4;
	p[2*5] = (void *)__call_lkl_ops_5;
	p[2*6] = (void *)__call_lkl_ops_6;
	p[2*7] = (void *)__call_lkl_ops_7;
	p[2*8] = (void *)__call_lkl_ops_8;
	p[2*9] = (void *)__call_lkl_ops_9;
	p[2*10] = (void *)__call_lkl_ops_10;
	p[2*11] = (void *)__call_lkl_ops_11;
	p[2*12] = (void *)__call_lkl_ops_12;
	p[2*13] = (void *)__call_lkl_ops_13;
	p[2*14] = (void *)__call_lkl_ops_14;
	p[2*15] = (void *)__call_lkl_ops_15;
	p[2*16] = (void *)__call_lkl_ops_16;
	p[2*17] = (void *)__call_lkl_ops_17;
	p[2*18] = (void *)__call_lkl_ops_18;
	p[2*19] = (void *)__call_lkl_ops_19;
	p[2*20] = (void *)__call_lkl_ops_20;
	p[2*21] = (void *)__call_lkl_ops_21;
	p[2*22] = (void *)__call_lkl_ops_22;
	p[2*23] = (void *)__call_lkl_ops_23;
	p[2*24] = (void *)__call_lkl_ops_24;
	p[2*25] = (void *)__call_lkl_ops_25;
	p[2*26] = (void *)__call_lkl_ops_26;
	p[2*27] = (void *)__call_lkl_ops_27;
	p[2*28] = (void *)__call_lkl_ops_28;
	p[2*29] = (void *)__call_lkl_ops_29;
	p[2*30] = (void *)__call_lkl_ops_30;
	p[2*31] = (void *)__call_lkl_ops_31;
	p[2*32] = (void *)__call_lkl_ops_32;
	p[2*33] = (void *)lkl_jmp_buf_set;
	p[2*34] = (void *)lkl_jmp_buf_longjmp;
	p[2*35] = (void *)__call_lkl_ops_35;
	p[2*36] = (void *)__call_lkl_ops_36;
	p[2*37] = (void *)__call_lkl_ops_37;
	p[2*38] = (void *)__call_lkl_ops_38;
	p[2*39] = (void *)&lkl_pci_ops_veneer;
}

__asm__(
".global __ret_from_hybrid_fn\n"
".type __ret_from_hybrid_fn, %function\n"
"__ret_from_hybrid_fn:\n"
	
	"mrs c16, rcsp_el0\n"
	"mov c17, c16\n"
	/* N.B. The compiler does not allow us to use 
	   C64 syntax even after a BX #4 instruction */
	"ldp c29, c30, [x16], #32\n"
	"ldp c27, c28, [x16], #32\n"
	"ldp c25, c26, [x16], #32\n"
	"ldp c23, c24, [x16], #32\n"
	"ldp c21, c22, [x16], #32\n"
	"ldp c19, c20, [x16], #32\n"

	"scvalue c16, c17, x16\n"
	"msr rcsp_el0, c16\n"
	"bx #4\n" /* Switch execution back to C64 */
	"retr c30\n" /* Return to restricted state */
	".size __ret_from_hybrid_fn, .-__ret_from_hybrid_fn\n"
);


/* setjmp and longjmp implementations have been copied here. 
 * Crucially, these implementatons are able to save both 
 * CSP and RCSP while running in Executive state.
 */
__asm__ (
".global lkl_setjmp\n"
".type lkl_setjmp, %function\n"
"lkl_setjmp:\n"
	"stp c19, c20, [x0,#0]\n"
	"stp c21, c22, [x0,#32]\n"
	"stp c23, c24, [x0,#64]\n"
	"stp c25, c26, [x0,#96]\n"
	"stp c27, c28, [x0,#128]\n"
	"stp c29, c30, [x0,#160]\n"
	"mrs c2, rcsp_el0\n"
	"str c2, [x0,#192]\n"
	"mov c2, csp\n"
	"str c2, [x0,#208]\n"
	"stp  d8,  d9, [x0,#224]\n"
	"stp d10, d11, [x0,#240]\n"
	"stp d12, d13, [x0,#256]\n"
	"stp d14, d15, [x0,#272]\n"
	"mov x0, #0\n"
	"ret\n"
".size lkl_setjmp, .- lkl_setjmp\n"
);

__asm__ (
".global lkl_longjmp\n"
".type lkl_longjmp, %function\n"
"lkl_longjmp:\n"
	"ldp c19, c20, [x0,#0]\n"
	"ldp c21, c22, [x0,#32]\n"
	"ldp c23, c24, [x0,#64]\n"
	"ldp c25, c26, [x0,#96]\n"
	"ldp c27, c28, [x0,#128]\n"
	"ldp c29, c30, [x0,#160]\n"
	"ldr c2, [x0,#192]\n"
	"msr rcsp_el0, c2\n"
	"ldr c2, [x0,#208]\n"
	"mov csp, c2\n"
	"ldp d8 , d9, [x0,#224]\n"
	"ldp d10, d11, [x0,#240]\n"
	"ldp d12, d13, [x0,#256]\n"
	"ldp d14, d15, [x0,#272]\n"

	"cmp w1, 0\n"
	"csinc w0, w1, wzr, ne\n"
	"ret\n"
".size lkl_longjmp, .- lkl_longjmp\n"
);
