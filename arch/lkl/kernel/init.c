// SPDX-License-Identifier: GPL-2.0-only
#include <asm/host_ops.h>
#include <asm/kasan.h>

extern void init_lkl_ops(void);
int lkl_init(struct lkl_host_operations *__capability ops)
{
	real_lkl_ops = ops;
	init_lkl_ops();
	lkl_ops = &lkl_ops_veneer;

	return kasan_init();
}

void lkl_cleanup(void)
{
	if (kasan_cleanup() < 0)
		lkl_printf("kasan: failed to cleanup\n");
}
