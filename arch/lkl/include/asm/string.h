/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _ASM_LKL_STRING_H
#define _ASM_LKL_STRING_H

#include <asm/types.h>
#include <asm/host_ops.h>

/* use __mem* names to avoid conflict with KASAN's mem* functions. */


#if defined(CONFIG_KASAN)

/*
 * For files that are not instrumented (e.g. mm/slub.c) we
 * should use not instrumented version of mem* functions.
 */
#if !defined(__SANITIZE_ADDRESS__)

#ifndef __NO_FORTIFY
#define __NO_FORTIFY /* FORTIFY_SOURCE uses __builtin_memcpy, etc. */
#endif

#else /* __SANITIZE_ADDRESS__ */

#undef memcpy
#undef memset
extern void *memset(void *dst, int c, __kernel_size_t count);
extern void *memcpy(void *dst, const void *src, __kernel_size_t count);

#endif /* __SANITIZE_ADDRESS__ */

#endif /* CONFIG_KASAN */

#endif /* _ASM_LKL_STRING_H */
