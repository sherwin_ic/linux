/* SPDX-License-Identifier: GPL-2.0 */
/*
 * Based on arch/arm64/include/asm/syscall_wrapper.h
 */

#ifndef __ASM_SYSCALL_WRAPPER_H
#define __ASM_SYSCALL_WRAPPER_H

#ifdef CONFIG_COMPAT

#define COMPAT_SYSCALL_DEFINE0(name)						\
	__COMPAT_SYSCALL_DEFINE_ARCH(x, name, __VA_ARGS__)			\
	asmlinkage long compat_sys_##name(void);				\
	ALLOW_ERROR_INJECTION(compat_sys_##name, ERRNO);			\
	asmlinkage long compat_sys_##name(void)

#define COMPAT_SYSCALL_DEFINEx(x, name, ...)					\
	__COMPAT_SYSCALL_DEFINE_ARCH(x, name, __VA_ARGS__)			\
	asmlinkage long compat_sys##name(__MAP(x,__SC_DECL,__VA_ARGS__))	\
		__attribute__((alias(__stringify(__se_compat_sys##name))));	\
	ALLOW_ERROR_INJECTION(compat_sys##name, ERRNO);				\
	static inline long __do_compat_sys##name(__MAP(x,__SC_DECL,__VA_ARGS__));\
	asmlinkage long __se_compat_sys##name(__MAP(x,__SC_LONG,__VA_ARGS__));	\
	asmlinkage long __se_compat_sys##name(__MAP(x,__SC_LONG,__VA_ARGS__))	\
	{									\
		long ret = __do_compat_sys##name(__MAP(x,__SC_DELOUSE,__VA_ARGS__));\
		__MAP(x,__SC_TEST,__VA_ARGS__);					\
		return ret;							\
	}									\
	static inline long __do_compat_sys##name(__MAP(x,__SC_DECL,__VA_ARGS__))

#endif /* CONFIG_COMPAT */

#define __SYSCALL_DEFINE0(sname, ret_type)					\
	SYSCALL_METADATA(sname, 0);						\
	__SYSCALL_DEFINE_ARCH(0, sname, ret_type);		                \
	asmlinkage ret_type sys##sname(void);	                                \
	ALLOW_ERROR_INJECTION(sys##sname, ERRNO);			        \
	asmlinkage ret_type sys##sname(void)

#define __SYSCALL_RET_T		long
#define __SYSCALL_RET_T_PTR	user_intptr_t

/*
 * Bit of playing around with variadic macros here....
 * It tweaks the SYSCALL_PREP to become a variadic macro and enable
 * inserting extra variable argument prior to __SYSCALL_DEFINEx
 * being fully evaluated (macro arguments are completely macro-expanded
 * before being actually placed in the macro body).
 * SYSCALL_PREP/__SYSCALL_ANNOTATE relies on the ability to leave
 * macro arguments empty which allows the __SYSCALL_ANNOTATE to
 * be properly expanded for cases where the type is not provided.
 * Note that __SYSCALL_ANNOTATE is required here to avoid syntax
 * errors (extra comma) in case ret_type is missing.
 * As variable arguments represent zero or more tokens until the closing
 * parenthesis, after expanding SYSCALL_PREP, the variadic argument
 * for the top-level macro will gain additional token placed before
 * arguments provided by any of the SYSCALL_DEFINE macros.
 *
 * To cut the long story short, it could be ilustrated as:
 * SYSCALL_DEFINE1(__retptr(syscall_name), arg_type, arg)
 * |-> SYSCALL_DEFINEx(1, SYSCALL_PREP(__retptr(syscall_name)), arg_type, arg)
 * |-> SYSCALL_DEFINEx(1, SYSCALL_PREP(syscall_name, _PTR), arg_type, arg)
 * |-> SYSCALL_DEFINEx(1, __SYSCALL_ANNOTATE(_syscall_name, _PTR), arg_type, arg)
 * |-> SYSCALL_DEFINEx(1, _syscall_name, __SYSCALL_RET_T_PTR, arg_type, arg)
 * \-> SYSCALL_DEFINEx(1, _syscall_name, user_intptr_t, arg_type, arg)
 *
 */
#define __retptr__(name) name, _PTR
#define __SYSCALL_ANNOTATE(name, ret_type) name, __SYSCALL_RET_T##ret_type
#define SYSCALL_PREP(name, ...) __SYSCALL_ANNOTATE(_##name, __VA_ARGS__)

/*
 * Some syscalls with no parameters return valid capabilities, so __SYSCALL_DEFINE0
 * is added to handle such cases.
 * __SYSCALL_DEFINE0 receives a pair of the annotated syscall name and its
 * return type due to the expansion of SYSCALL_PREP(name). Note that
 * __SYSCALL_DEFINE0 is concatenating its macro arguments with other tokens, so
 * SYSCALL_PREP(name) wouldn't have been expanded if it was passed directly to it.
 * Therefore the intermediate helper macro __SYSCALL_DEFINE0_ANNOTATED is used to
 * allow SYSCALL_PREP(name) to be expanded.
 */
#define __SYSCALL_DEFINE0_ANNOTATED(name) __SYSCALL_DEFINE0(name)
#define SYSCALL_DEFINE0(name) __SYSCALL_DEFINE0_ANNOTATED(SYSCALL_PREP(name))



#define __SYSCALL_DEFINEx(x, name, ret_type, ...)				\
	asmlinkage ret_type sys##name(__MAP(x,__SC_DECL,__VA_ARGS__))		\
		__attribute__((alias(__stringify(__se_sys##name))));		\
	ALLOW_ERROR_INJECTION(sys##name, ERRNO);				\
	static inline ret_type __do_sys##name(__MAP(x,__SC_DECL,__VA_ARGS__));	\
	asmlinkage ret_type __se_sys##name(__MAP(x,__SC_LONG,__VA_ARGS__));	\
	asmlinkage ret_type __se_sys##name(__MAP(x,__SC_LONG,__VA_ARGS__))	\
	{									\
		ret_type ret = __do_sys##name(__MAP(x,__SC_CAST,__VA_ARGS__));	\
		__MAP(x,__SC_TEST,__VA_ARGS__);					\
		__PROTECT(x, ret,__MAP(x,__SC_ARGS,__VA_ARGS__));		\
		return ret;							\
	}									\
	static inline ret_type __do_sys##name(__MAP(x,__SC_DECL,__VA_ARGS__))


#endif /* __ASM_SYSCALL_WRAPPER_H */