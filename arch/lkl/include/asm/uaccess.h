/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * Based on arch/arm/include/asm/uaccess.h
 *
 * Copyright (C) 2012 ARM Ltd.
 */
#ifndef __ASM_UACCESS_H
#define __ASM_UACCESS_H

#define __get_kernel_nofault(dst, src, type, err_label)	\
do {							\
	if (0)						\
		goto err_label;				\
	*(type *)dst = *(type *)src;			\
} while (0)

#define __put_kernel_nofault __get_kernel_nofault

static inline __must_check unsigned long
raw_copy_from_user(void *to, const void __user *from, unsigned long n)
{
	unsigned long length;
	unsigned long remaining;
	unsigned char *dst;
	const unsigned char *__capability src;
	length = __builtin_cheri_length_get(from)
				- __builtin_cheri_offset_get(from);
	remaining = 0;
	if (length < n) {
		remaining = n - length;
		n = length;
	}
	dst = to;
	src = from;
	for (; n; --n) *dst++ = *src++;
	return remaining;
}

static inline __must_check unsigned long
raw_copy_to_user(void __user *to, const void *from, unsigned long n)
{
	unsigned long length;
	unsigned long remaining;
	unsigned char *__capability dst;
	const unsigned char *src;
	length = __builtin_cheri_length_get(to)
				- __builtin_cheri_offset_get(to);
	remaining = 0;
	if (length < n) {
		remaining = n - length;
		n = length;
	}
	dst = to;
	src = from;
	for (; n; --n) *dst++ = *src++;
	return remaining;
}

/**
 * Similar to arm64 __arch_copy_{to/from}_user_with_captags, this first copies any first few non 16-bit aligned bytes
 * until src is 16-bits aligned. Then if dst is also 16-bits aligned at this point data is copied as if they were
 * capabilities. This means that copying with captags only occurs if both src and dst have the same alignment in the
 * beginning.
 */

static inline __must_check unsigned long
raw_copy_from_user_with_captags(void *to, const void __user * from, unsigned long n)
{
	unsigned char *dst = to;
	const unsigned char *__capability src = from;
	const uintcap_t *__capability src_aligned;
	uintcap_t *dst_aligned;

	unsigned alignment_offset = __builtin_align_up(src, 16) - src;
	for (; alignment_offset; --alignment_offset) {
		*dst++ = *src++;
		--n;
	}
	
	if (!__builtin_is_aligned(dst, 16) || !__builtin_is_aligned(src, 16))
		return raw_copy_from_user(dst, src, n);

	src_aligned = (const uintcap_t *__capability) src;
	dst_aligned = (uintcap_t *) dst;
	while (n > 15) {
		*dst_aligned++ = *src_aligned++;
		n -= 16;
	}
	src = (const unsigned char *__capability) src_aligned;
	dst = (unsigned char *) dst_aligned;
	for (; n; --n) *dst++ = *src++;
	return 0;
}
#define raw_copy_from_user_with_captags raw_copy_from_user_with_captags	

static inline __must_check unsigned long
raw_copy_to_user_with_captags(void __user *to, const void *from, unsigned long n)
{
	unsigned char *__capability dst = to;
	const unsigned char *src = from;
	const uintcap_t *src_aligned;
	uintcap_t *__capability dst_aligned;

	unsigned alignment_offset = __builtin_align_up(src, 16) - src;
	for (; alignment_offset; --alignment_offset) {
		*dst++ = *src++;
		--n;
	}
	
	if (!__builtin_is_aligned(dst, 16) || !__builtin_is_aligned(src, 16))
		return raw_copy_to_user(dst, src, n);

	src_aligned = (const uintcap_t *) src;
	dst_aligned = (uintcap_t *__capability) dst;
	while (n > 15) {
		*dst_aligned++ = *src_aligned++;
		n -= 16;
	}
	src = (const unsigned char *) src_aligned;
	dst = (unsigned char *__capability) dst_aligned;
	for (; n; --n) *dst++ = *src++;
	return 0;
}
#define raw_copy_to_user_with_captags raw_copy_to_user_with_captags	

static inline unsigned long __must_check __clear_user(void __user *to, unsigned long n)
{
	unsigned char __capability *d = to;
	for (; n; n--) *d++ = 0;
	return n;
}
#define __clear_user __clear_user

static __always_inline int
__put_user_ptr_fn(void * __capability * __capability ptr,
		  void * __capability *x)
{
	return unlikely(raw_copy_to_user_with_captags(ptr, x, sizeof(*x))) ?
		-EFAULT : 0;
}
#define __put_user_ptr_fn(u, k)	__put_user_ptr_fn(u, k)

#define __put_user_ptr(x, ptr)							\
({										\
	void __user *__x = (void __user *)(x);					\
	__chk_user_ptr(ptr);							\
	__put_user_ptr_fn((void * __capability * __capability)ptr, &__x);	\
})

#define __get_user_ptr(x, ptr)						\
({									\
	raw_copy_from_user_with_captags(&(x), ptr, sizeof(*ptr));	\
})
#define get_user_ptr	__get_user_ptr

#include <asm-generic/uaccess.h>

#endif /* __ASM_UACCESS_H */
