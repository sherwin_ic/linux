#ifndef _ASM_LKL_SYSCALLS_H
#define _ASM_LKL_SYSCALLS_H

int syscalls_init(void);
void syscalls_cleanup(void);
long lkl_syscall(long no, uintcap_t *params);
void wakeup_idle_host_task(void);

asmlinkage user_intptr_t sys_open(uintcap_t,uintcap_t,uintcap_t,uintcap_t,uintcap_t,uintcap_t);
asmlinkage user_intptr_t sys_close(uintcap_t,uintcap_t,uintcap_t,uintcap_t,uintcap_t,uintcap_t);

#define sys_mmap sys_mmap_pgoff
#define sys_mmap2 sys_mmap_pgoff
#define sys_clone sys_ni_syscall
#define sys_vfork sys_ni_syscall
#define sys_rt_sigreturn sys_ni_syscall

#include <asm-generic/syscalls.h>

#endif /* _ASM_LKL_SYSCALLS_H */
